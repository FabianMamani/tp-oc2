#include <stdio.h>
#include <stdlib.h>

extern void formulaResolvente(float a,float b,float c);

int main() {
	//int a,b,c
	float a,b,c;
	//Tomamos por consola los valores ingresados para A,B y C
	printf("Ingrese los valores A,B y C para realizar el calculo de la formula resolvente:");

	printf("Valor A: ");
    scanf("%f", &a);
	printf("Valor B: ");
    scanf("%f", &b);
	printf("Valor C: ");
    scanf("%f", &c);
	formulaResolvente(a,b,c);
	return 0;
}