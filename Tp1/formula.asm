extern printf
global formulaResolvente

section .data

;variables para los terminos y coheficientes
bnegativo dd 0.0 ;(-b)
bcuadrado dd 0.0 ;(bxb -> b al cuadrado)
multiplicacion dd 0.0 ;(4ac -> Multiplicacion dentro de la raiz)
divisor dd 0.0 ; (2a -> divisor)
discriminante dd 0.0 ;(valor de la raiz, puede llegar a indicar la cantidad de raices obtenidas)

;Calculo posibles raices
calculoSuma dd 0.0 ;(valor del termino suma)
calculoResta dd 0.0;(valor del termino resta)

;Resultados obtenidoz
raizX1 dq 0.0 ;(raiz x1)
raizX2 dq 0.0 ;(raiz x2)


; variables auxiliares para los calculos
variableAux1 dd 4.0
variableAux2 dd 2.0

;Msg de salida
msg db "La raiz 1 es: %.2f y la raiz 2 es: %.2f  ", 10,13,0

section .text 
 
formulaResolvente:
    push ebp ; 
    mov ebp, esp  ; 
    
    ;Parametros de entrada desde main.c:
    ;coheficiente a=EBP+8
    ;coheficiente b=EBP+12
    ;coheficiente c=EBP+16
    
    ;calulamos el valor -B
    FLD dword[EBP+12]
    FCHS ;cambiamos al signo opuesto
    FSTP dword[bnegativo] ; guardamos la variable
    
    ;calculamos B al cuadrado 
    FLD dword[EBP+12]
    FLD dword[EBP+12]
    FMUL ;multiplicacion el mismo valor
    FSTP dword[bcuadrado] ;guardamos en variable
    
    ;calculamos la multiplicacion 4xaxc
    FLD dword[variableAux1] ;almacenamos la variableAux1
    FLD dword[EBP+8] ;almacenamos variable a
    FMUL ;realizamos la multiplicacion
    FLD dword[EBP+16] ;almacenamos la variable c
    FMUL ;realizamos la multiplicacion
    FSTP dword[multiplicacion]  ;guardamos la variable
    
    ;calculamos el divisor(2xa)
    FLD dword[variableAux2] ;Almacenamos el valor 2 
    FLD dword[EBP+8];almacenamos el coheficiente a 
    FMUL ;realizamos multiplicacion
    FSTP dword[divisor] ;guardamos variables
    
    ;calculamos determinante (BxB - 4*a*c)
    FLD dword[bcuadrado]    ;almacenamos BxB
    FLD dword[multiplicacion]    ;almacenamos la multiplicacion 4xaxc
    FSUB ;realizamos la resta
    FSQRT ;calculamos la raiz cuadra a la resta anterior
    FSTP dword[discriminante] ;guardamos la variable
    

    ;Iniciamos el calculo de los términos(suma y resta)
    ;realizaremos primeramente la carga del coheficiente b(-b) en la pila 
    ;y posteriormente cargamos el resultado del discriminante obtenido
    ;Seguidamente haremos el calculo para ambas ramas de la formula para obtener las raices.

    ;calculo término con suma 
    FLD dword[bnegativo] ;almacenamos -b en la pila
    FLD dword[discriminante] ;almacenamos el valor de la discriminante calculada
    FADD  ;realizamos la suma
    FSTP dword[calculoSuma] ;guardamos la variable
    
    ;calculo término con resta
    FLD dword[bnegativo] ;almacenamos el valor -b
    FLD dword[discriminante] ;almacenamos el valor de la discriminante calculada
    FSUB ;realizamos la resta
    FSTP dword[calculoResta] ;guardamos la variable.
    

    ;Vamos a calcular el cociente de los resultados obtenidos en ambos flujos por el divisor calculado(2a)
    ;Guardaremos las raices obtenidas en las variables raizX1 y raizX2
    
    FLD dword[calculoSuma]
    FLD dword[divisor] 
    FDIV ;(calculoSuma/divisor)
    FSTP qword[raizX1] ;guardo en la variable

    FLD dword[calculoResta]
    FLD dword[divisor] 
    FDIV ;hago: (calculoResta/divisor)
    FSTP qword[raizX2] ;guardo en la variable
    
    ;Imprimimos las raices obtenidas
    push dword[raizX2+4]
    push dword[raizX2]
    push dword[raizX1+4]
    push dword[raizX1]
    push msg
    call printf 
    add esp,20 
    
    
    mov ebp,esp ;Reseteamos la Stack
    pop ebp ;Restauramos
    
    ret

;Queda pendiente armar las llamadas a subrutinas para atomizar los calculos

;Queda pendiente ver una subrutina para el calculo de cantidad de raices posibles
;si el discriminante >0 -> 2 soluciones distintas
;si el discriminante <0 -> Una solucion doble
;si el discriminante =0 -> No tiene solucion real