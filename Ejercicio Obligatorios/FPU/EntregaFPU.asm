%include "io.inc"
extern _printf
section .data
vector dd 5.23,3.14,1.66,40.23,132.09
cantidad dd 5
sumaTotal dq 0.0
msg db "El resultado del vector es : %f", 10,13,0
section .text
global CMAIN
CMAIN:
    mov ebp, esp; 
    xor ebx,ebx ;
    push cantidad
    push vector
    call suma_vf ;Llamamos a la funcion
    add esp,8 
    
    ;Imprimo por pantalla
    push dword[sumaTotal+4]
    push dword[sumaTotal]  
    push msg
    call printf 
    add esp,12 
    
    xor eax, eax

    ret
    
suma_vf:
    
    push ebp 
    mov ebp, esp  
    
    mov eax , [EBP+8] ;en eax tengo la direccion del vector
    mov edx , [EBP+12];en edx tengo la direccion de la cantidad de numeros que tengo en el vector
    
    
    ;iniciamos la suma de los dos primero valores dentro del ciclo
    FLD dword[eax]
    inc ebx ;incremento el contador
    
ciclo: 
    FLD dword[eax +4*ebx]
    FADDP ;sumo los dos primeros numeros
    inc ebx 
    ;Verificamos si la cantidad de numeros ya se recorrio
    cmp ebx, [edx]
    jl ciclo 
    
    
 
    fst qword[sumaTotal] ;guardamos el valor en sumaTotal
    
    mov ebp,esp 
    pop ebp
    
    ret
