_**Alumno: Mamani Fabián Gustavo TP OC II**_

_**Formula Resolvente**_
    
    El Tp consiste en poder integrar un archivo asm al cual estaremos llamando desde otro archivo creado 
    en lenguaje C. El TP se basará en implementar la formula Resolvente que se aplica en los polinomios de grado 2. 
    Para ello tendremos solicitaremos datos desde un archivo main.c el cual estará realizando una llamada a una función externa
    implementada en Assembler.
    A continuación dejaremos los pasos a seguir para poder compilar el programa.


**Ejecución**

Contamos con el archivo "script.sh" en el cual tendremos el siguiente contenido:

_En esta linea indicamos al sistema que es un archivo bash_

#!/bin/bash

_Creamos el archivo formula.asm_en un objeto_

    nasm -f elf32 formula.asm -o resolvente.o

_Linkeamos el objeto creado para compilarlo junto con un archivo.c y obtenemos un ejecutable_

    sudo gcc -m32 main.c resolvente.o -o ejecutable

_Ejecutamos el archivo ejecutable_

    ./ejecutable


**Importante**

Es posible que al querer ejecutar el archivo bash nos solicite, ademas de una contraseña, que le otorguemos permiso de ejecución al archivo script.sh lo cual lo haremos con la siguiente linea:

    chmod +x script.sh






_**Entregas de Practica de Memoria y FPU**_

La entregas de los ejercicios obligatorios se encuentran en la carpeta "Ejercicios Obligatorios" del presente repositorio.
En el mismo podemos encontrar los siguientes ejercicios solicitados:
1. Gestión de Memoria
2. FPU 
